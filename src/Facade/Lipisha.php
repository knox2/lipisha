<?php

namespace Knox\Lipisha\Facade;

use Illuminate\Support\Facades\Facade;

class Lipisha extends Facade
{
    protected static function getFacadeAccessor() { return 'lipisha'; }
}