<?php

namespace Knox\Lipisha;

use Illuminate\Support\ServiceProvider;

class LipishaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {        
        $this->publishes([
            __DIR__.'/Config/lipisha.php' => config_path('lipisha.php'),
        ], 'lipisha');

        if (! $this->app->routesAreCached()) {
            require __DIR__.'/routes.php';
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/Config/lipisha.php', 'lipisha'
        );

        $this->mergeConfigFrom(
            __DIR__.'/Config/config.php', 'lipisha'
        );

        $this->app->singleton('lipisha',function($app) {
            return new Lipisha;
        });
    }
}
