<?php

return [
    'api_version' => '1.3.0',

    'api_type' => 'Callback',

    'live_site' => 'https://api.lipisha.com/v2/',

    'demo_site' => 'http://developer.lipisha.com/index.php/v2/api/',

];
