<?php

return [
    'api_key' => env('LIPISHA_APIKEY'),

    'api_signature' => env('LIPISHA_APISIGNATURE'),

    'live' => env('LIPISHA_LIVE', true),

    'currency' => env('LIPISHA_CURRENCY', 'KES')
];
