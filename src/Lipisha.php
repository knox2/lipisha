<?php

namespace Knox\Lipisha;

class Lipisha
{
    public function getBalance(){
        return $this->request('get_balance');
    }

    public function getFloat($account = ""){
        return $this->request('get_float', ['account_number' => $account]);
    }

    public function requestMoney($account = "", $mobile = "", $amount = "", $reference = "", $currency = "KES", $method = "Paybill (M-Pesa)"){
        return $this->request('request_money', ['account_number' => $account, 'mobile_number' => $mobile, 'amount' => $amount, 'reference' => $reference, 'currency' => $currency, 'method' => $method]);
    }

    public function sendMoney($account = "", $mobile = "", $amount = "", $reference = "", $currency = "KES"){
        return $this->request('send_money', ['account_number' => $account, 'mobile_number' => $mobile, 'amount' => $amount, 'reference' => $reference, 'currency' => $currency]);
    }

    public function sendAirtime($account = "", $mobile = "", $amount = "", $reference = "", $currency = "KES"){
        return $this->request('send_airtime', ['account_number' => $account, 'mobile_number' => $mobile, 'amount' => $amount, 'reference' => $reference, 'currency' => $currency]);
    }

    public function sendSMS($account = "", $mobile = "", $message = "", $reference = ""){
        return $this->request('send_sms', ['account_number' => $account, 'mobile_number' => $mobile, 'message' => $message, 'reference' => $reference]);
    }

    public function authorizeCard($account = "", $card_number = "", $expiry = "", $security_code = "", $name = "", $country = "", $state = "", $zip = "", $address1 = "", $amount = "", $address2 = ""){
        return $this->request('authorize_card_transaction', ['api_version' => config('lipisha.api_version'), 'api_type' => config('lipisha.api_type'), 'account_number' => $account, 'card_number' => $card_number,'email' => 'test@email.com', 'mobile_number' => '0711223344', 'expiry' => $expiry, 'security_code' => $security_code, 'name' => $name, 'country' => $country, 'state' => $state, 'zip' => $zip, 'address1' => $address1, 'amount' => $amount, 'address2' => $address2, 'currency' => config('lipisha.currency')]);
    }

    public function completeCardTransaction($transaction_index = "", $transaction_reference = ""){
        return $this->request('complete_card_transaction', ['api_version' => config('lipisha.api_version'), 'api_type' => config('lipisha.api_type'), 'transaction_index' => $transaction_index, 'transaction_reference' => $transaction_reference]);
    }

    public function reverseCardTransaction($transaction_index = "", $transaction_reference = ""){
        return $this->request('reverse_card_transaction', ['api_version' => config('lipisha.api_version'), 'api_type' => config('lipisha.api_type'), 'transaction_index' => $transaction_index, 'transaction_reference' => $transaction_reference]);
    }
    

    public function request($action, $params = array()){
        
        $url = config('lipisha.live') == true ? config('lipisha.live_site') : config('lipisha.demo_site');

        $url.=$action;

        $api = array(
            'api_key' => config('lipisha.api_key'),
            'api_signature' => config('lipisha.api_signature')
        );

        $fields = http_build_query(array_merge($api, $params));
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        
        return json_decode($result);
    }
    
}